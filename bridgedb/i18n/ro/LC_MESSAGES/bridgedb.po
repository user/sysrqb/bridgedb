# Translations template for bridgedb.
# Copyright (C) 2020 'The Tor Project, Inc.'
# This file is distributed under the same license as the bridgedb project.
# 
# Translators:
# A C <ana@shiftout.net>, 2019
# Adda.17 <inactive+Adda.17@transifex.com>, 2013
# Adrian Staicu <astaicu@gmail.com>, 2017
# Ana <ana_maria_js@yahoo.com>, 2015
# axel_89, 2015
# Di N., 2015
# eduard pintilie <eduard.pintilie@gmail.com>, 2019-2020
# Isus Satanescu <inactive+isus@transifex.com>, 2014
# laura berindei <lauraagavriloae@yahoo.com>, 2014
# polearnik <polearnik@mail.ru>, 2019
# clopotel <yo_sergiu05@yahoo.com>, 2014
msgid ""
msgstr ""
"Project-Id-Version: Tor Project\n"
"Report-Msgid-Bugs-To: 'https://trac.torproject.org/projects/tor/newticket?component=BridgeDB&keywords=bridgedb-reported,msgid&cc=isis,sysrqb&owner=isis'\n"
"POT-Creation-Date: 2020-05-14 14:21-0700\n"
"PO-Revision-Date: 2020-05-15 08:24+0000\n"
"Last-Translator: Transifex Bot <>\n"
"Language-Team: Romanian (http://www.transifex.com/otf/torproject/language/ro/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.8.0\n"
"Language: ro\n"
"Plural-Forms: nplurals=3; plural=(n==1?0:(((n%100>19)||((n%100==0)&&(n!=0)))?2:1));\n"

#. -*- coding: utf-8 ; test-case-name: bridgedb.test.test_https_server -*-
#. This file is part of BridgeDB, a Tor bridge distribution system.
#. :authors: please see included AUTHORS file
#. :copyright: (c) 2007-2017, The Tor Project, Inc.
#. (c) 2013-2017, Isis Lovecruft
#. :license: see LICENSE for licensing information
#. : The path to the HTTPS distributor's web templates.  (Should be the
#. : "templates" directory in the same directory as this file.)
#. Setting `filesystem_checks` to False is recommended for production servers,
#. due to potential speed increases. This means that the atimes of the Mako
#. template files aren't rechecked every time the template is requested
#. (otherwise, if they are checked, and the atime is newer, the template is
#. recompiled). `collection_size` sets the number of compiled templates which
#. are cached before the least recently used ones are removed. See:
#. http://docs.makotemplates.org/en/latest/usage.html#using-templatelookup
#. : A list of supported language tuples. Use getSortedLangList() to read this
#. variable.
#. We use our metrics singleton to keep track of BridgeDB metrics such as
#. "number of failed HTTPS bridge requests."
#. Convert all key/value pairs from bytes to str.
#. TRANSLATORS: Please DO NOT translate the following words and/or phrases in
#. any string (regardless of capitalization and/or punctuation):
#. "BridgeDB"
#. "pluggable transport"
#. "pluggable transports"
#. "obfs4"
#. "Tor"
#. "Tor Browser"
#: bridgedb/distributors/https/server.py:154
msgid "Sorry! Something went wrong with your request."
msgstr "Scuze ! Ceva n-a mers cum trebuie!"

#: bridgedb/distributors/https/templates/base.html:42
msgid "Language"
msgstr "Limbă"

#: bridgedb/distributors/https/templates/base.html:94
msgid "Report a Bug"
msgstr "Raportează un bug"

#: bridgedb/distributors/https/templates/base.html:97
msgid "Source Code"
msgstr "Cod sursă"

#: bridgedb/distributors/https/templates/base.html:100
msgid "Changelog"
msgstr "Schimbări"

#: bridgedb/distributors/https/templates/bridges.html:35
msgid "Select All"
msgstr "Selectare totală"

#: bridgedb/distributors/https/templates/bridges.html:40
msgid "Show QRCode"
msgstr "Arată QRcod-ul"

#: bridgedb/distributors/https/templates/bridges.html:52
msgid "QRCode for your bridge lines"
msgstr "Cod QR pentru punțile tale"

#: bridgedb/distributors/https/templates/bridges.html:63
msgid "It seems there was an error getting your QRCode."
msgstr "Se pare că s-a produs o eroare în obținerea codului QR."

#: bridgedb/distributors/https/templates/bridges.html:68
msgid ""
"This QRCode contains your bridge lines. Scan it with a QRCode reader to copy"
" your bridge lines onto mobile and other devices."
msgstr "Codul QR conține punțile tale. Scanează-l cu un cititor de coduri QR pentru a copia liniile tale de punte pe mobil sau alt device."

#: bridgedb/distributors/https/templates/bridges.html:110
msgid "BridgeDB encountered an error."
msgstr ""

#: bridgedb/distributors/https/templates/bridges.html:116
msgid "There currently aren't any bridges available..."
msgstr "Pe moment nu există punți disponibile..."

#: bridgedb/distributors/https/templates/bridges.html:118
#: bridgedb/distributors/https/templates/bridges.html:122
#, python-format
msgid ""
" Perhaps you should try %s going back %s and choosing a different bridge "
"type!"
msgstr "Poate ar trebui să %s mergi înapoi %s și să alegi un alt tip de punte!"

#: bridgedb/distributors/https/templates/index.html:11
#, python-format
msgid "Step %s1%s"
msgstr "Pasul %s1%s"

#: bridgedb/distributors/https/templates/index.html:13
#, python-format
msgid "Download %s Tor Browser %s"
msgstr "Descarcă %s Tor Browser %s"

#: bridgedb/distributors/https/templates/index.html:25
#, python-format
msgid "Step %s2%s"
msgstr "Pasul %s2%s"

#: bridgedb/distributors/https/templates/index.html:28
#: bridgedb/distributors/https/templates/index.html:30
#, python-format
msgid "Get %s bridges %s"
msgstr "Obține %s punți %s"

#: bridgedb/distributors/https/templates/index.html:40
#, python-format
msgid "Step %s3%s"
msgstr "Pasul %s3%s"

#: bridgedb/distributors/https/templates/index.html:43
#: bridgedb/distributors/https/templates/index.html:47
#, python-format
msgid "Now %s add the bridges to Tor Browser %s"
msgstr "Acum %s adaugă punțile în Tor Browser %s"

#. TRANSLATORS: Please make sure the '%s' surrounding single letters at the
#. beginning of words are present in your final translation. Thanks!
#. (These are used to insert HTML5 underlining tags, to mark accesskeys
#. for disabled users.)
#: bridgedb/distributors/https/templates/options.html:42
#, python-format
msgid "%sJ%sust give me bridges!"
msgstr "%sD%soar dă-mi punțile odată!"

#: bridgedb/distributors/https/templates/options.html:55
msgid "Advanced Options"
msgstr "Opțiuni avansate"

#: bridgedb/distributors/https/templates/options.html:93
msgid "No"
msgstr "Nu"

#: bridgedb/distributors/https/templates/options.html:94
msgid "none"
msgstr "niciunul/niciuna"

#. TRANSLATORS: Please make sure the '%s' surrounding single letters at the
#. beginning of words are present in your final translation. Thanks!
#. TRANSLATORS: Translate "Yes!" as in "Yes! I do need IPv6 addresses."
#: bridgedb/distributors/https/templates/options.html:131
#, python-format
msgid "%sY%ses!"
msgstr "%sD%sa!"

#. TRANSLATORS: Please make sure the '%s' surrounding single letters at the
#. beginning of words are present in your final translation. Thanks!
#. TRANSLATORS: Please do NOT translate the word "bridge"!
#: bridgedb/distributors/https/templates/options.html:154
#, python-format
msgid "%sG%set Bridges"
msgstr "%sO%sbține Bridges"

#: bridgedb/strings.py:33
msgid "[This is an automated email.]"
msgstr "[Acesta este un e-mail trimis automat.]"

#: bridgedb/strings.py:35
msgid "Here are your bridges:"
msgstr "Acestea sunt punțile tale:"

#: bridgedb/strings.py:37
#, python-format
msgid ""
"You have exceeded the rate limit. Please slow down! The minimum time between\n"
"emails is %s hours. All further emails during this time period will be ignored."
msgstr "Ai depășit rata limită. Te rugăm să o iei mai încet! Timpul minim între\ne-mail-uri este %s ore. Toate e-mail-urile în acest interval vor fi ignorate."

#: bridgedb/strings.py:40
msgid ""
"If these bridges are not what you need, reply to this email with one of\n"
"the following commands in the message body:"
msgstr "Dacă aceste punți nu sunt cele necesare, răspundeți la acest e-mail cu una dintre\nurmătoarele comenzi în corpul mesajului:"

#. TRANSLATORS: Please DO NOT translate "BridgeDB".
#. TRANSLATORS: Please DO NOT translate "Pluggable Transports".
#. TRANSLATORS: Please DO NOT translate "Tor".
#. TRANSLATORS: Please DO NOT translate "Tor Network".
#: bridgedb/strings.py:50
#, python-format
msgid ""
"BridgeDB can provide bridges with several %stypes of Pluggable Transports%s,\n"
"which can help obfuscate your connections to the Tor Network, making it more\n"
"difficult for anyone watching your internet traffic to determine that you are\n"
"using Tor.\n"
"\n"
msgstr "BridgeDB poate oferi punți cu diferite tipuri de %sPluggable Transports %s,\nce pot ascunde conexiunile tale către Tor Network, făcând mai dificil \nde observat că folosești Tor\npentru cine îți urmărește traficul de Internet.\n\n"

#. TRANSLATORS: Please DO NOT translate "Pluggable Transports".
#: bridgedb/strings.py:57
msgid ""
"Some bridges with IPv6 addresses are also available, though some Pluggable\n"
"Transports aren't IPv6 compatible.\n"
"\n"
msgstr "Unele punți cu adrese IPv6 sunt disponibile, deși unele Pluggable\nTransports nu sunt compatibile cu IPv6.\n"

#. TRANSLATORS: Please DO NOT translate "BridgeDB".
#. TRANSLATORS: The phrase "plain-ol'-vanilla" means "plain, boring,
#. regular, or unexciting". Like vanilla ice cream. It refers to bridges
#. which do not have Pluggable Transports, and only speak the regular,
#. boring Tor protocol. Translate it as you see fit. Have fun with it.
#: bridgedb/strings.py:66
#, python-format
msgid ""
"Additionally, BridgeDB has plenty of plain-ol'-vanilla bridges %s without any\n"
"Pluggable Transports %s which maybe doesn't sound as cool, but they can still\n"
"help to circumvent internet censorship in many cases.\n"
"\n"
msgstr "În plus, BridgeDB are multe punți simple %s fără niciun\nTransport Conectabil %s ce poate nu par așa cool, dar care pot\ntotuși să ajute la evitarea cenzurii Internetului în multe cazuri.\n\n"

#: bridgedb/strings.py:78 bridgedb/test/test_https.py:356
msgid "What are bridges?"
msgstr "Ce sunt punțile? "

#: bridgedb/strings.py:79
#, python-format
msgid "%s Bridges %s are Tor relays that help you circumvent censorship."
msgstr "%s Punțile %s sunt releuri Tor care te ajută să eviți cenzura."

#: bridgedb/strings.py:84
msgid "I need an alternative way of getting bridges!"
msgstr "Am nevoie de o cale alternativă de a obține punțile!"

#. TRANSLATORS: Please DO NOT translate "get transport obfs4".
#: bridgedb/strings.py:86
#, python-format
msgid ""
"Another way to get bridges is to send an email to %s. Leave the email subject\n"
"empty and write \"get transport obfs4\" in the email's message body. Please note\n"
"that you must send the email using an address from one of the following email\n"
"providers: %s or %s."
msgstr "Un alt mod de a obține punți este să trimiteți un e-mail către %s. Lăsați subiectul de e-mail\ngol și scrieți  \"get transport obfs4\" în corpul de e-mail. Vă rugăm să rețineți\ncă trebuie să trimiteți e-mailul utilizând o adresă de la unul dintre furnizorii de e-mail: %s sau %s."

#: bridgedb/strings.py:94
msgid "My bridges don't work! I need help!"
msgstr "Punțile mele nu funcționează! Am nevoie de ajutor!"

#. TRANSLATORS: Please DO NOT translate "Tor Browser".
#. TRANSLATORS: The two '%s' are substituted with "Tor Browser Manual" and
#. "Support Portal", respectively.
#: bridgedb/strings.py:98
#, python-format
msgid ""
"If your Tor Browser cannot connect, please take a look at the %s and our %s."
msgstr "Dacă Tor Browser nu se poate conecta, vă rugăm să aruncați o privire la %s și %s."

#: bridgedb/strings.py:102
msgid "Here are your bridge lines:"
msgstr "Acestea sunt liniile tale de punți:"

#: bridgedb/strings.py:103
msgid "Get Bridges!"
msgstr "Obține punți!"

#: bridgedb/strings.py:107
msgid "Bridge distribution mechanisms"
msgstr "Mecanisme de distribuție a punții"

#. TRANSLATORS: Please DO NOT translate "BridgeDB", "HTTPS", and "Moat".
#: bridgedb/strings.py:109
#, python-format
msgid ""
"BridgeDB implements four mechanisms to distribute bridges: \"HTTPS\", \"Moat\",\n"
"\"Email\", and \"Reserved\".  Bridges that are not distributed over BridgeDB use\n"
"the pseudo-mechanism \"None\".  The following list briefly explains how these\n"
"mechanisms work and our %sBridgeDB metrics%s visualize how popular each of the\n"
"mechanisms is."
msgstr "BridgeDB implementează patru mecanisme de distribuire a punților: \"HTTPS\", \"Moat\",\n\"Email\" și \"Reserved\".  Punțile care nu sunt distribuite prin BridgeDB folosesc\npseudo-mecanismul \"None\".  Următoarea listă explică pe scurt cum  funcționează acestea\nși cum se poate vizualiza cu %sBridgeDB metrics%s cât de popular este fiecare \nmecanism."

#: bridgedb/strings.py:115
#, python-format
msgid ""
"The \"HTTPS\" distribution mechanism hands out bridges over this website.  To get\n"
"bridges, go to %sbridges.torproject.org%s, select your preferred options, and\n"
"solve the subsequent CAPTCHA."
msgstr "Mecanismul de distribuție „HTTPS” transmite punți peste acest site web.  Pentru a obține\npunți, accesați %sbridges.torproject.org%s, selectați opțiunile pe care le preferați și\nrezolvați testul CAPTCHA."

#: bridgedb/strings.py:119
#, python-format
msgid ""
"The \"Moat\" distribution mechanism is part of Tor Browser, allowing users to\n"
"request bridges from inside their Tor Browser settings.  To get bridges, go to\n"
"your Tor Browser's %sTor settings%s, click on \"request a new bridge\", solve the\n"
"subsequent CAPTCHA, and Tor Browser will automatically add your new\n"
"bridges."
msgstr "Mecanismul de distribuire \"Moat\" face parte din Tor Browser, permițând utilizatorilor să\nsolicite punți din setările Tor Browser.  Pentru a obține punți, accesați\nsetările Tor Browser %sTor settings%s, clic pe \"request a new bridge\", rezolvați\ntestul CAPTCHA și Tor Browser va adăuga automat noile\npunți."

#: bridgedb/strings.py:125
#, python-format
msgid ""
"Users can request bridges from the \"Email\" distribution mechanism by sending an\n"
"email to %sbridges@torproject.org%s and writing \"get transport obfs4\" in the\n"
"email body."
msgstr "Utilizatorii pot solicita punți de la mecanismul de distribuție „Email” prin trimiterea unui\nemail la %sbridges@torproject.org%s și scriind \"get transport obfs4\" în\ncorpul email-ului."

#: bridgedb/strings.py:129
msgid "Reserved"
msgstr "Rezervat"

#: bridgedb/strings.py:130
#, python-format
msgid ""
"BridgeDB maintains a small number of bridges that are not distributed\n"
"automatically.  Instead, we reserve these bridges for manual distribution and\n"
"hand them out to NGOs and other organizations and individuals that need\n"
"bridges.  Bridges that are distributed over the \"Reserved\" mechanism may not\n"
"see users for a long time.  Note that the \"Reserved\" distribution mechanism is\n"
"called \"Unallocated\" in %sbridge pool assignment%s files."
msgstr "BridgeDB menține un număr mic de punți care nu sunt distribuite\nautomat.  În schimb, rezervăm aceste punți pentru distribuirea manuală și\nle înmânăm unor ONG-uri și unor organizații și persoane fizice care au nevoie\nde punți.  Punțile care sunt distribuite prin mecanismul \"Reserved\" ar putea să nu \nvadă utilizatorii pe o perioadă mai mare de timp.  Rețineți că mecanismul de distribuire \"Reserved\" este\nnumit \"Unallocated\" în fișierele %sbridge pool assignment%s."

#: bridgedb/strings.py:137
msgid "None"
msgstr "Nimic"

#: bridgedb/strings.py:138
msgid ""
"Bridges whose distribution mechanism is \"None\" are not distributed by BridgeDB.\n"
"It is the bridge operator's responsibility to distribute their bridges to\n"
"users.  Note that on Relay Search, a freshly set up bridge's distribution\n"
"mechanism says \"None\" for up to approximately one day.  Be a bit patient, and\n"
"it will then change to the bridge's actual distribution mechanism.\n"
msgstr "Punțile al căror mecanism de distribuție este „None” nu sunt distribuite de BridgeDB.\nEste responsabilitatea operatorului de punți să distribuie punțile lor către\nutilizatori. Rețineți că la căutarea cu releu, distribuția unei punți proaspăt configurate\nmecanismul răspunde cu \"None\" timp de aproximativ o zi.  Aveți răbdare, pentru că\nse va schimba apoi la mecanismul de distribuție propriu-zis al punții.\n"

#: bridgedb/strings.py:148
msgid "Please select options for bridge type:"
msgstr "Alege opțiunile pentru tipul de punte:"

#: bridgedb/strings.py:149
msgid "Do you need IPv6 addresses?"
msgstr "Ai nevoie de adrese IPv6?"

#: bridgedb/strings.py:150
#, python-format
msgid "Do you need a %s?"
msgstr "Ai nevoie de %s?"

#: bridgedb/strings.py:154
msgid "Your browser is not displaying images properly."
msgstr "Browserul nu afișează imaginile corect."

#: bridgedb/strings.py:155
msgid "Enter the characters from the image above..."
msgstr "Introdu caracterele din imaginea de mai sus..."

#: bridgedb/strings.py:159
msgid "How to start using your bridges"
msgstr "Cum să începi să foloseşti punțile"

#. TRANSLATORS: Please DO NOT translate "Tor Browser".
#: bridgedb/strings.py:161
#, python-format
msgid ""
" First, you need to %sdownload Tor Browser%s. Our Tor Browser User\n"
" Manual explains how you can add your bridges to Tor Browser. If you are\n"
" using Windows, Linux, or OS X, %sclick here%s to learn more. If you\n"
" are using Android, %sclick here%s."
msgstr " Mai întâi, trebuie să  %sdescărcați Tor Browser%s. Manualul Tor Browser User\nexplică cum puteți adăuga punți la Tor Browser. Dacă\n folosiți Windows, Linux sau OS X, %sclic aici%s pentru a afla mai multe. Dacă\n folosiți Android, %sclic aici%s."

#: bridgedb/strings.py:166
msgid ""
"Add these bridges to your Tor Browser by opening your browser\n"
"preferences, clicking on \"Tor\", and then adding them to the \"Provide a\n"
"bridge\" field."
msgstr "Adăugați aceste punți la Tor Browser deschizând \npreferințele pentru browserul, faceți clic pe „Tor”, apoi adăugați-le la câmpul „Provide a\nbridge\"."

#: bridgedb/strings.py:173
msgid "(Request unobfuscated Tor bridges.)"
msgstr "(Solicită punți Tor nedisimulate - unobfuscated.)"

#: bridgedb/strings.py:174
msgid "(Request IPv6 bridges.)"
msgstr "(Cere punți IPv6.)"

#: bridgedb/strings.py:175
msgid "(Request obfs4 obfuscated bridges.)"
msgstr "(Cere punți obfuscate obfs4.)"
